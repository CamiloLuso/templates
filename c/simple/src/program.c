#include <stdio.h>
#include "module/depend.h"
#include "module/no_depend.h"

int main(int argc, char ** argv) {
	printf("program!\n");

	print_depend();
	printf("\n");

	print_no_depend();
	printf("\n");

	return 0;
}
