# Project Name

Description.

## Purpose

> The purpose of this project...

# Project Structure

- `src/:` source modules.
- `bin/:` object and binary files.
- `lib/:` external libraries.
- `doc/:` documentation.
	- `ANALYSIS:` functional models (US, UC, SRS, SyRS...), structural models (domain models).
	- `DESIGN:` design alternatives, design decisions.
- `test/:` test code.
- `.gitignore:` ignored files for Git.
- `Makefile:` build rules for Make.
- `README.md:` project sumary.

# Usage

## Compile

```bash
> make
```

## Export

```bash
> make export
```

## Execute

```bash
> make run
```

# Notes

1. You can add more directories or files within a different structure.
2. You can use whatever format for "ANALYSIS" and "DESIGN".
3. You should follow "Makefile" rules for simplicity.
4. You should change "Makefile" to fit your project.
5. You should change this document to fit your project.
