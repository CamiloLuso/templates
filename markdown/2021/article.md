---
lang: es-ES
numbersections: true
link-citations: true
header-includes:
    \usepackage[a4paper, margin=1in]{geometry}
    \setlength{\parskip}{1em}
    \pagestyle{plain}

    \usepackage{fontspec}
    \setmainfont[Ligatures=TeX, Scale=1.2]{Libertinus Serif}
    \setmathfont{Libertinus Math}

    \usepackage{fancyvrb}
---

\title{ {\Huge \textbf{Asignatura \\ P1 -- Nombre de práctica}} }

\author{ \textbf{Camilo Caparrós Laiz} \\ \texttt{camilo.caparrosl@um.es}
         \and \textbf{Nombre Apellido Apellido} \\ \texttt{correo@web.dom}}

\date{ \vspace{8em} \textbf{Fecha} \\ Abril 1999 \\
       \vspace{2em} \textbf{Grupo} \\ Grupo 1.1 \\
       \vspace{2em} \textbf{Profesor} \\ Nombre Apellido Apellido}

\maketitle
\newpage

\tableofcontents
\newpage

# Sección

El siguiente párrafo es solo para rellenar como todo documento en general[@zzz], necesita tener mínimo una cantidad de letras, palabras o páginas. Esto hace que pierda su valor inconmesurablemente[@camilo].

# Referencias
