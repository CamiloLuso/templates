---
lang: es-ES
numbersections: true
header-includes:
    \newcommand{\hideFromPandoc}[1]{#1}
    \hideFromPandoc{
        \let\Begin\begin
        \let\End\end
    }

    \usepackage[a4paper, margin=1in]{geometry}
    \setlength{\parskip}{1em}
    \pagestyle{plain}

    \usepackage{fontspec}
    \setmainfont[Ligatures=TeX, Scale=1.2]{Libertinus Serif}


    \usepackage{fancyvrb}
    \usepackage{multicol}

    \usepackage{xcolor}
    \definecolor{blue}{RGB}{33, 150, 243}
    \definecolor{red}{RGB}{233, 30, 99}
    \definecolor{yellow}{RGB}{251, 192, 45}
    \definecolor{green}{RGB}{76, 175, 80}
    \definecolor{purple}{RGB}{103, 58, 183}
---

\title{ {\Huge \textbf{Libro}} }
\author{\textbf{Camilo Caparrós Laiz} \\ \texttt{correo@web.dom}
\and \textbf{Nombre Apellido Apellido} \\ \texttt{correo@web.dom}}
\date{ \textbf{Fecha} \\ Abril 1999}
\maketitle
\newpage

\tableofcontents
\newpage

# Sección

Texto normal *cursiva* **negrita** ~~tachado~~ 2^10^ 2~1~ \*\*Texto escapado\*\*. Qué.

## Subsección

Texto normal de subsección.

### Subsubsección

Texto normal de subsubsección.

#### Párrafo

Texto normal de párrafo.

##### Subpárrafo

Texto normal de subpárrafo.

# Listas

Sin orden:

- Elemento.
- Elemento.
- Elemento.

---

Con orden:

1. Elemento.
2. Elemento.
3. Elemento.

# Tablas

| $A$ | $B$ | $A \Rightarrow B$ |
| :-: | :-: | :---------------: |
| $V$ | $V$ | $V$ |
| $V$ | $F$ | $F$ |
| $F$ | $V$ | $V$ |
| $F$ | $F$ | $V$ |

| **Actividad** | **L** | **M** | **X** | **J** | **V** | **S** | **D** | **Total** |
| :-----------  | :---: | :---: | :---: | :---: | :---: | :---: | :---: | --------: |
| **Dormir** | 9h | 9h | 9h | 9h | 9h | 9h | 9h | 63h |
| **Transporte** | 3h | 3h | 3h | 3h | | | | 12h |
| **Universidad** | 5h | 5h | 5h | 5h | | | | 20h |
| **Estudiar** | | 4h | 2h | 4h | 6h | 6h | 8h | 30h |
||
| **Total** | 17h | 21h | 19h | 21h | 15h | 15h | 17hh | 125h |

# Fórmulas matemáticas

$$\bar{x} = \frac{1}{n} \sum_{i = 1}^{n} (x_i)$$

$$\int_a^b x \cdot dx = \frac{x^2}{2}$$

$$\forall x: P(x) \wedge x \in \mathbb{Z}$$

# Código

Código **_inline_**: la variable `l` es de tipo `List<String>`.

Código escrito en el **documento directamente**:

```C
#include <stdio.h>

int main(int argc, char ** argv) {
	return 0;
}
```

Código escrito en **fichero**:

`\VerbatimInput{code/main.c}`

Código escrito en **fichero**, pero especifica **rango**:

`\VerbatimInput[firstline=1, lastline=3]{code/main.c}`

# Imágenes

`![Imagen](img/imagen.png)`

# Columnas

\Begin{multicols}{2}

**Paso por valor**

```C
void f(int x) {
	x = 4;
}

...

x = 1;
f(x);
/* x = 1 */
```

**Paso por referencia**

```C
void f(int * x) {
	*x = 4;
}

...

x = 1;
f(&x);
/* x = 4 */
```

\End{multicols}

# Texto citado

Esta oración tiene una nota[^nota]. El siguiente texto [\ref{chapuza}] está citado.

[^nota]: No hay nota.

# Referencias

\label{chapuza} [\ref{chapuza}] Autor, Título. Extras. Chapuza. [www.url.com](www.url.com)
