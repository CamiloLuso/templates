---
lang: es-ES
numbersections: true
header-includes:
    \usepackage[a4paper, margin=1in]{geometry}
    \setlength{\parskip}{1em}
    \pagestyle{plain}

    \usepackage{fontspec}
    \setmainfont[Ligatures=TeX, Scale=1.2]{Libertinus Serif}
    \setmathfont{Libertinus Math}

    \usepackage{fancyvrb}
    \usepackage{multicol}
reference-section-title: Referencias
link-citations: true
references:
- type: article
  id: camilo
  author: {family: Caparrós Laiz, given: Camilo}
  issued: {year: 1999}
  title: Mein Kampf
  page: 1-2
  URL: www.url.com
- type: article
  id: cita
  author: {family: Apellido Apellido, given: Nombre}
  issued: {year: 1999}
  title: Título
  page: 1-2
  URL: www.url.com
---

\title{ {\Huge \textbf{Asignatura \\ P1 -- Nombre de práctica}} }
\author{ \textbf{Camilo Caparrós Laiz} \\ \texttt{camilo.caparrosl@um.es}
         \and \textbf{Nombre Apellido Apellido} \\ \texttt{correo@web.dom}}
\date{ \vspace{8em} \textbf{Fecha} \\ Abril 1999 \\
       \vspace{2em} \textbf{Grupo} \\ Grupo 1.1 \\
       \vspace{2em} \textbf{Profesor} \\ Nombre Apellido Apellido}
\maketitle
\newpage

\tableofcontents
\newpage

# Sección

Texto normal *cursiva* **negrita** ~~tachado~~ 2^10^ 2~1~ \*\*Texto escapado\*\*. Qué.

## Subsección

Texto normal de subsección.

### Subsubsección

Texto normal de subsubsección.

#### Párrafo

Texto normal de párrafo.

##### Subpárrafo

Texto normal de subpárrafo.

# Listas

Sin orden:

- Elemento.
- Elemento.
- Elemento.

---

Con orden árabe:

1. Elemento.
2. Elemento.
3. Elemento.

Con orden romano:

i. Elemento.
ii. Elemento.
iii. Elemento.

Con orden alfabético:

a. Elemento.
b. Elemento.
c. Elemento.

# Tablas

| **Actividad** | **L** | **M** | **X** | **J** | **V** | **S** | **D** | **Total** |
| :-----------  | :---: | :---: | :---: | :---: | :---: | :---: | :---: | --------: |
| **Dormir** | 9h | 9h | 9h | 9h | 9h | 9h | 9h | 63h |
| **Transporte** | 3h | 3h | 3h | 3h | | | | 12h |
| **Universidad** | 5h | 5h | 5h | 5h | | | | 20h |
| **Estudiar** | | 4h | 2h | 4h | 6h | 6h | 8h | 30h |
||
| **Total** | 17h | 21h | 19h | 21h | 15h | 15h | 17hh | 125h |

# Fórmulas matemáticas

$$\bar{x} = \frac{1}{n} \sum_{i = 1}^{n} (x_i)$$

$$\int_{a}^{b} x \cdot dx = \frac{x^2}{2}$$

$$\forall x: P(x) \wedge x \in \mathbb{Z}$$

# Código

## _inline_

La variable `l` es de tipo `List<String>`.

## Documento

```C
#include <stdio.h>

int main(int argc, char ** argv) {
	return 0;
}
```

## Fichero

### Completo

`\VerbatimInput{code/main.c}`

### Parcial

`\VerbatimInput[firstline=1, lastline=3]{code/main.c}`

# Imágenes

`![Imagen](img/imagen.png)`

# Texto citado

Este texto tiene una nota[^nota]. Este texto [@camilo] está citado. Este texto también [@cita].

[^nota]: No hay nota.
